Homework 2 - Report
===================

##How to build and run##
sbt is configured so that 'sbt package' downloads and builds everything into
target.
The compiled jar which works on the cluster is loganalysis_2.10-1.0.jar and is
located at the root level of the folder.

To run the program simply use 'spark-submit --class LogAnalysis --master yarn-client path/to/jar --num-executor X'
I created a small and convenient shell script to run and print the output, which
is 'make.sh' at the root of the project.


##Journey##
At first I used the basic the basic parsing example with the hope to extend it
later but it quickly became obvious that time would be a critical resource for
this homework, so I decided in the end only to rely on the fileds of the app
summary (excepted for the final status, of course). However, by extending the
parser I believe all the fields I use could be extracted from logs that occurred
before the first exception.
The values given are the area under Reciever Operating Characteristic (auROC)
which I understood thanks to a website listed in the references below.

The feature I first used was the running time of the job, which did not that
good (auROC = ~0.51, quite as good as random...). I then decided to explore
other features such as the start time. One last feature I added was the username
because I think different people used different coding strategies (e.g. testing
everything on the cluster from the early, buggy code vs only final testing).
These three features (execution time, start time and username) are the three
ones I decided to keep because they, all together, yield the best auROC.

While yielding improvements on the auROC, these did not
enhanced it significantly. I then turned to different models for fitting the
data. The first one was LinearRegressionWithSGD, and did not seem to perform
good. I turned to SVMWithSGD, which gave better results but still unsatisfying
ones: ~0.58. I then tried different regression algorithms (linear and ridged)
but those did not improved the prediction either, even by playing with the
parameters.

One last category of algorithms I wanted to try out was the tree-based one. I
first used GradientBoostedTreesModel and then tried RandomForest, but the
latter gave worse results that the former (~0.82 vs ~0.91, respectively).

The values given for the auROC is the one obtained with the best parameters,
but one last question occurred: the tree-based solutions, while giving better
results, are quite slow, especially for large iteration values. So besides the
max depth value (for which the value of 5 gives the best results) and the
boosting strategy which works bets using "Regression", I wanted to explore how
the iteration count would change the auROC in order to explore the
time/goodness tradeoff. It turned out 100 iterations are not good at all, since
it takes more than 15 minutes to complete, while giving an average auROC of
0.9557 versus 0.9466 for the 10-iteration version, which runs in a little more
than two minutes. It the depends on how the ML algorithm is applied, for a task
that needs precision I would recommend using the 100 iteration version but for
the homework's purpose I believe 10 (as in the code I propose) or 20 are
sufficient.

##Time taken##
The overall time this task took me was roughly 12 hours, which is why I decided
not to extend the parser

##References##
Informations on YARN's exceptions:
https://cwiki.apache.org/confluence/display/CLOUDSTACK/Exceptions+and+logging
The useful API:
http://spark.apache.org/docs/latest/api/scala/#package
Examples on mllib linear methods
http://spark.apache.org/docs/1.2.1/mllib-linear-methods.html
Examples on how to use tree algorithms:
https://spark.apache.org/docs/latest/mllib-ensembles.html
Details on the ROC:
http://gim.unmc.edu/dxtests/roc3.htm
