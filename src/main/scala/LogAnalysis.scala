import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics

//import org.apache.spark.mllib.regression.RidgeRegressionWithSGD
//import org.apache.spark.mllib.classification.SVMWithSGD
//import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.tree.model.GradientBoostedTreesModel
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import org.apache.spark.mllib.tree.GradientBoostedTrees

object LogAnalysis {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("LogAnalysis"))

    //val lines = sc.textFile("../log_samples/yarn-yarn-resourcemanager-master1.log*") // local files
    val lines = sc.textFile("hdfs:///datasets/clusterlogs/yarn-yarn-resourcemanager-master1.log*") // HDFS files

    // parse them to get AppSummary
    def parseLine(l: String): LogLine =
      LogP.parse(LogP.logline, l).getOrElse(UnknownLine())

    def statusToDouble(status: String) = status match {
      case "SUCCEEDED" =>
        1.0
      case _ =>
        0.0
    }

    def nameToDouble(name:String) = {
      name.hashCode()
    }

    def f(a: LogLine) = a match {
      case AppSummary(_,_,_,user,_,_,_,startTime,endTime,status) =>
        List(LabeledPoint(
          statusToDouble(status),
          Vectors.dense(Array(
            endTime.toDouble-startTime.toDouble,
            startTime.toDouble,
            nameToDouble(user)
          ))
        ))
      case _ => List()
    }

    val parsedData = lines.map(l => parseLine(l)).flatMap(f).cache()

    // used to perform the training + testing procedure five times
    def trainAndTest() = {
      // Split data into training (60%) and test (40%).
      val splits = parsedData.randomSplit(Array(0.6, 0.4))
      val training = splits(0)
      val test = splits(1)

      // Train a GradientBoostedTrees model.
      // The defaultParams for Classification use LogLoss by default.
      val boostingStrategy = BoostingStrategy.defaultParams("Regression")
      boostingStrategy.numIterations = 10
      boostingStrategy.treeStrategy.numClasses = 2
      boostingStrategy.treeStrategy.maxDepth = 5
      // Empty categoricalFeaturesInfo indicates all features are continuous.
      boostingStrategy.treeStrategy.categoricalFeaturesInfo = Map[Int, Int]()

      val model = GradientBoostedTrees.train(training, boostingStrategy)
    
      // Clear the default threshold.
      //model.clearThreshold()

      // Compute raw scores on the test set.
      val scoreAndLabels = test.map { point =>
        val score = model.predict(point.features)
        (score, point.label)
      }
      val metrics = new BinaryClassificationMetrics(scoreAndLabels)
      metrics.areaUnderROC()
    }

    val size = 5
    val result = List.fill[Double](size)(0.0)
    val auROC = result.map(x => trainAndTest).foldLeft(0.0)(_+_) / size
    println(auROC)

    sc.stop()
  }
}
