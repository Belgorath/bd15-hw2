#!/bin/sh

sbt $1 package &&
spark-submit --class LogAnalysis --master yarn-client target/scala-2.10/loganalysis_2.10-1.0.jar --num-executor 25 >> out 
echo ""
echo "---------------------------"
echo "   Content of out file:"
echo "---------------------------"
cat out
echo "---------------------------"



